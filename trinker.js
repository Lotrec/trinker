const { filter, push } = require("./people");

module.exports = tr = {
    title: function (){
        return `
$$$$$$$$\\        $$\\           $$\\                           $$\\                               
\\__$$  __|       \\__|          $$ |                          $$ |                              
   $$ | $$$$$$\\  $$\\ $$$$$$$\\  $$ |  $$\\  $$$$$$\\   $$$$$$\\  $$ | $$$$$$\\ $$\\    $$\\  $$$$$$\\  
   $$ |$$  __$$\\ $$ |$$  __$$\\ $$ | $$  |$$  __$$\\ $$  __$$\\ $$ |$$  __$$\\\\$$\\  $$  |$$  __$$\\ 
   $$ |$$ | \\__|$$ |$$ |  $$ |$$$$$$  / $$$$$$$$ |$$ |  \\__|$$ |$$ /  $$ |\\$$\\$$  / $$$$$$$$ |
   $$ |$$ |      $$ |$$ |  $$ |$$  _$$    $$   ____|$$ |      $$ |$$ |  $$ | \\$$$  /  $$   ____|
   $$ |$$ |      $$ |$$ |  $$ |$$ | \\$$\\ \\$$$$$$$\\ $$ |$$\\   $$ |\\$$$$$$  |  \\$  /   \\$$$$$$$\\ 
   \\__|\\__|      \\__|\\__|  \\__|\\__|  \\__| \\_______|\\__|\\__|  \\__| \\______/    \\_/     \\_______|
================================================================================================
   `.yellow
    },

    line: function(title = "="){
        return title.padStart(48, "=").padEnd(96, "=").bgBrightYellow.black
    },

    allMale: function(p){
        // permet de donner tous les hommes en entrant people
        return p.filter(i=>i.gender==='Male');
    },

    allFemale: function(p){
        // permet de donnert toutes les femmes en entrant people
        return p.filter(i=>i.gender==='Female');
    },

    nbOfMale: function(p){
        // tu rentre "people" et ça sort le nbre de mecs
        return this.allMale(p);
    },

    nbOfFemale: function(p){
         // tu rentre "people" et ça sort le nbre de femme
        return this.allFemale(p);
    },

    nbOfMaleInterest: function(p){
         // tu rentre "people" et ça sort le nbre de gens intéréssés par les hommes
        return p.filter(i=>i.looking_for==='M');
    },

    nbOfFemaleInterest: function(p){
         // tu rentre "people" et ça sort le nbre de gens intérésés par les femmes
        return p.filter(i=>i.looking_for==='F');
    },

    

    nbOfPeopleLikeDrama: function(p){
         // tu rentre "people" et ça sort le nbre de gensqui aiment QUE les drama
        return p.filter(i=>i.pref_movie.includes ("Drama")).length
    },
    nbOfFemaleLikeSF: function(p){
         // tu rentre "people" et ça sort le nbre de femmes qui aiment la sf
        return this.allFemale(p).filter(i=>i.pref_movie.includes ("Sci-Fi")).length
    },

    nbPplLikeDocAnd1482Dolls: function(p){
         // tu rentre "people" et ça sort le nbre de gens qui aiment les documentaires et qui gagnent 1482 dollars
    var Thunasse = p.filter(k => this.getIncome(k) > 1482.00);
        return Thunasse.filter(i=>i.pref_movie.includes ("Documentary")).length;
    },
   
    listNPIDincome: function(i){
        // sert à ressortir l'id, le nom, prénom et le salaire (en mode FBI bro)
        return (i.id+i.first_name+i.last_name+i.income);
    },

    // listeNPIDincomeSup: function(p, n){
    //     return this.nbIncomeSup(p, n).map(this.listNPIDincome);
    // },
// au dessus ça marche donc PAS TOUCHER OU TU CASSE TOUT COUILLON


// LA fonction ultime de la mort qui tue nécesssaire à tout et qui fait un chiffre de tout ça
    nb: function (people, Filtre, param){
        let Filtré = Filtre(people, param);
        return Filtré.length;
    },

    ByGender: function (p, g){
         // tu rentre "people" et le genre (Male,Female) ça sort les personnes concernées 
        return p.filter(i=>i.gender=== g)
    },

    ByInterest: function (p, t){
            // tu rentre "people" et leur attirance (M,F) ça sort les personnes concernées 
        return p.filter(i=>i.looking_for===t)
    },

    Bylastname: function (p, t){
            // tu rentre "people" et le nom ('nom') ça sort la personne concernée
        return p.filter(i=>i.last_name===t)
    },

    ByMovie: function (p, m){
        // tu rentre "people" et le film('film') ça sort les personnes concernées
        return p.filter(i=>i.pref_movie.includes (m))
    },


    ByIncome: function(p, n){
        // tu rentre "people" et le salaire (1000.00) ça sort les personnes concernées gagant moins
        return p.filter(i=> tr.getIncome(i) > n); 
        },
    
        
    getIncome: function(i){
        //  sert à séparer le dollar ($) du chiffre dans le tableau et le lire en tant que chiffre et pas en texte (string)
            return parseFloat(i.income.slice(1));
        },

    ByTotalID: function(i){
        // affiche toute la donnée nom, prénom , id et salaire
            return (i.id+i.first_name+i.last_name+i.income);
        },

    ByLatitudeNord: function (p){
        // tu rentre "people" et ça sort les personnes vivant dans l'hémisphère nord
        return p.filter(i=> i.latitude > 0)
    },

    ByLatitudeSud: function (p){
        // tu rentre "people" et ça sort les personnes vivant dans l'hémisphère sud
        return p.filter(i=> i.latitude < 0)
    },

    nbListage: function (people, Filtre, param){
        // fait tout comme nb plus haut mais affiche toute la donnée nom, prénom , id et salaire
            let Filtré = Filtre(people, param);
            return Filtré.map(this.ByTotalID);
        },

//   réfléchis, donc prends de l'eau ducon et pose ton cerveau
//   pour trouver le plus riche, utilise .sort avec a-b pour faire un tri
// il faut d'abord prendre les salaires PUIS .sort pour le tri
// il faudra extraire, donc vérifie le .slice ça pourrrait être utile.
//  donc ce soir, tu bosses.
// oh et j'y pense, faut faire la fonction dans la fonction (poupées russes)

    
getIncome: function(i){
        return parseFloat(i.income.slice(1));
    },

    richestGuy: function(p,){
        // permet de trouver le mec le plus riche, tu rentre "people" et pouf
        let x = this.ByGender(p, "Male");
        let salaire = [] ;
        for(let elem of x){
            tampon = parseFloat(elem.income.slice(1))
            salaire.push([tampon, elem.id, elem.last_name])
        }
        salaire.sort(function(a, b) {
            return a[0] - b[0];
          });
        return salaire[salaire.length-1]
    },


ByAllIncome: function (p, n){
    // pour filtrer par salaire, tu rentre people et un chiffre et ca  sort ceux qui touchent moins
    return p.filter(i=> tr.getIncome(i) > n);
},

nbListageThune: function (people, Filtre, param){
    let Filtré = Filtre(people, param);
    return Filtré.map(this.ByAfficheThune);
},

ByAfficheThune: function(i){
    return (i.income);
},

averageS: function(p){
    // fonction de salaire moyen de groupe, tu rentre people et pouf
    let avarage = 0
    for(let elem of p){
        avarage += parseFloat(elem.income.slice(1))
    }
    return avarage/p.length
},

nbMoyenne: function (a) {
    var b = a.length,
        c = 0, i;
    for (i = 0; i < b; i++){
      c += Number(a[i]);
    }
    return (c/b);
  },


  mediane: function (p){
    // sort le salaire médian du groupe
    tab = []
    for(let elem of p){
        avarage = parseFloat(elem.income.slice(1))
      tab.push(avarage)
    }
    tab.sort((a, b)=>
        a - b)
          let median;
          if(tab.length%2 != 0){ //sert verification si nombre pair ou impaire !=inegalité
              //c'est impaire
              let middleIndex = Math.floor(tab.length/2) //trouve le milieu de l'index

              median = tab[middleIndex] // le median egal le milieu de l'index
          }else{
              //si c'est pair
              let middleIndex = Math.floor(tab.length/2) // trouve le milieu de l'index

              median = (tab[middleIndex] + tab[middleIndex - 1])/2
              return median // calcul le median 

  }
},


AlderDuBresil: function(p){
    // c'est pour filtrer les salaires des gens de l'hémisphère sud, tu rentre juste people
        let sudpeople = p.filter(p => p.latitude < 0)
        let moyenne = 0 //declare moyenne qui part de 0
        for (let elem of sudpeople){ // POUR ( un element de p)
            moyenne += parseFloat(elem.income.slice(1)) // on ajoute tout les Float de Income à moyenne 0
        }
        return moyenne/sudpeople.length // on divise le resultat de moyenne par la taille du tableau
    },

    // EP III : LA REVANCHE DES SITES



    distanceEntre:function(p1,p2){
        // sert à calculer la distance entre deux points via pythagore
        let a = p1.latitude-p2.latitude
        let b = p1.longitude-p2.longitude
        let d = Math.sqrt(Math.pow(a, 2)+ Math.pow(b,2))
        return d
    },
    
    persPlusProches: function(people, cible){
        // permet de trouver les personnes les plus proches, tu rentre (people, people[num id])[distance] );
        let t = []
        for(let p of people){
            let d = this.distanceEntre(p, cible)
            t.push({personne: p, distance: d})
        }
        t = t.sort((a, b)=> a.distance - b.distance)
       return t
    },
        tenclosestojoseeBernard: function(p){
        // trouve les 10 gens proches , tu rentre People et voilà
        r= []
                       for (let i of p){
                        p1 ={x:11.9704401 , y:57.6938555}
                        p2 ={x:i.longitude , y:i.latitude}

                        a = p2.y - p1.y
                        b = p2.x - p1.x
                        c = Math.sqrt ((a*a)+(b*b))

                            let simple = { 
                                latitude : i.latitude, longitude : i.longitude, 
                                id : i.id, name : i.first_name +" "+ i.last_name,
                                distance : c
                            };
                            r.push(simple)
                        }
                     return r.slice(1, 11) 
                        
                },

        Les23gensdeGoogle: function (p){
            // tout est dans le nom, ca récupe les gens qui bossent chez google
        r= []
        for (let i of p){
         
             let simple = { 
                 latitude : i.latitude, longitude : i.longitude, 
                 id : i.id, name : i.first_name +" "+ i.last_name,
                 email : i.email
             };
             r.push(simple)
         }
      return r.filter( i=> i.email.includes("google"))
        },



        // Ehpad: function(p){
            // fonction qui fonctionne pas
        //     r= []
        //     for (let i of p){
             
        //          let simple = { 
        //              latitude : i.latitude, longitude : i.longitude, 
        //              id : i.id, name : i.first_name +" "+ i.last_name,
        //              date_of_birth : i.date_of_birth
                    
        //          };
        //          r.push(simple);
                

        //          r.sort(function(a, b) {
        //             return a[0] - b[0];
        //           });
        //         return r[r.length-1]

        //      }
        //   return r.map(this.ByTotalID);
        
        
        // },


    // LE BOSS FINAL

    match: function(p){
        // fonction inutile car il y à rien 
        return "not implemented".red;
    },
}
